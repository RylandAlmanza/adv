#include "interface.h"

int get_color_pair(int fg, int bg) {
    return fg + (bg * 8) + 1;
}

int initialize_all_color_pairs() {
    int fg;
    int bg;
    for (bg = 0; bg < 8; bg++) {
        for (fg = 0; fg < 8; fg++) {
            init_pair(get_color_pair(fg, bg), fg, bg);
        }
    }
}

void adv_display_init() {
    initscr();
    cbreak();
    keypad(stdscr, TRUE);
    noecho();
    curs_set(0);
    start_color();
    initialize_all_color_pairs();
}

void adv_display_draw(char character, int x, int y, int fg, int bg) {
    int attribute = COLOR_PAIR(get_color_pair(fg, bg));
    attron(attribute);
    mvaddch(y, x, character);
    attroff(attribute);
}

void adv_display_draw_string(char *str, int x, int y, int fg, int bg) {
    int attribute = COLOR_PAIR(get_color_pair(fg, bg));
    attron(attribute);
    mvprintw(y, x, str);
    attroff(attribute);
}

void adv_display_flush() {
    refresh();
}

void adv_display_wipe() {
    clear();
}

void adv_display_uninit() {
    curs_set(1);
    endwin();
}

adv_display adv_display_new() {
    adv_display display;

    display.init = &adv_display_init;
    display.draw = &adv_display_draw;
    display.draw_string = &adv_display_draw_string;
    display.uninit = &adv_display_uninit;
    display.flush = &adv_display_flush;
    display.wipe = &adv_display_wipe;

    return display;
}
