#include "scene.h"

void adv_set_scene(void (*init)(adv_display *display),
                   void (*update)(adv_display *display, int key),
                   void (*uninit)()) {
    adv_init_scene = init;
    adv_update_scene = update;
    adv_uninit_scene = uninit;
}
