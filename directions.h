#ifndef DIRECTIONS_H_
#define DIRECTIONS_H_

#include "point.h"
#include "actions.h"

static const int adv_north = 0;
static const int adv_east = 1;
static const int adv_south = 2;
static const int adv_west = 3;
static const int adv_invalid_direction = 4;

static const adv_point adv_direction_deltas[] = {
    {.x = 0, .y = -1},
    {.x = 1, .y = 0},
    {.x = 0, .y = 1},
    {.x = -1, .y = 0}
};

static const int adv_reverse_directions[] = {
    2,
    3,
    0,
    1
};

static const int adv_key_to_direction(int key) {
    if (key == adv_action_move_up) {
        return adv_north;
    } else if (key == adv_action_move_right) {
        return adv_east;
    } else if (key == adv_action_move_down) {
        return adv_south;
    } else if (key == adv_action_move_left) {
        return adv_west;
    } else {
        return adv_invalid_direction;
    }
}

#endif
