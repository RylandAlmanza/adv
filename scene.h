#ifndef SCENE_H_
#define SCENE_H_

#include "interface.h"

void (*adv_init_scene)(adv_display *display);
void (*adv_update_scene)(adv_display *display, int key);
void (*adv_uninit_scene)();

void adv_set_scene(void (*init)(adv_display *display),
                   void (*update)(adv_display *display, int key),
                   void (*uninit)());

#endif
