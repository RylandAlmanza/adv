#include "entity.h"
#include "directions.h"
#include "luautils.h"

lua_State *adv_entity_L;

void adv_entity_init() {
    adv_entity_L = luaL_newstate();
    adv_lua_init_state(adv_entity_L, "entities.lua");
}

void adv_entity_update(adv_entity *self) {

}

void adv_entity_move(adv_entity *self, int direction) {
    self->x += adv_direction_deltas[direction].x;
    self->y += adv_direction_deltas[direction].y;
    self->facing = direction;
}

void adv_entity_move_back(adv_entity *self) {
    self->facing = adv_reverse_directions[self->facing];
    self->move(self, self->facing);
}

adv_entity adv_entity_get(char *entity_name) {
    adv_entity entity;
    entity.x = 0;
    entity.y = 0;
    entity.facing = adv_north;
    entity.update = &adv_entity_update;
    entity.move = &adv_entity_move;
    entity.move_back = &adv_entity_move_back;

    adv_lua_select(adv_entity_L, "entities");
    adv_lua_select(adv_entity_L, entity_name);
    entity.is_solid = adv_lua_get_bool_field(adv_entity_L, "is_solid");
    adv_lua_select(adv_entity_L, "sprite");
    entity.sprite.character = adv_lua_get_str_field(adv_entity_L,
                                                    "character")[0];
    entity.sprite.fg = adv_lua_get_int_field(adv_entity_L, "fg");
    entity.sprite.bg = adv_lua_get_int_field(adv_entity_L, "bg");
    lua_pop(adv_entity_L, 1);
    lua_pop(adv_entity_L, 1);
    lua_pop(adv_entity_L, 1);

    return entity;
}

adv_entity adv_tile_get(char tile_symbol) {
    char tile_symbol_string[2];
    char *entity_name;

    tile_symbol_string[0] = tile_symbol;
    tile_symbol_string[1] = 0;

    adv_lua_select(adv_entity_L, "tile_symbols");
    entity_name = adv_lua_get_str_field(adv_entity_L, tile_symbol_string);
    lua_pop(adv_entity_L, 1);

    return adv_entity_get(entity_name);
}

void adv_entity_uninit() {
    adv_lua_uninit_state(adv_entity_L);
}
