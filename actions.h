#ifndef ACTIONS_H_
#define ACTIONS_H_

static const int adv_action_move_up = 1;
static const int adv_action_move_right = 2;
static const int adv_action_move_down = 3;
static const int adv_action_move_left = 4;
static const int adv_action_select = 5;

#endif
