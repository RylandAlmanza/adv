#ifndef POINT_H_
#define POINT_H_

typedef struct adv_point_struct adv_point;

struct adv_point_struct {
    int x;
    int y;
};

#endif
