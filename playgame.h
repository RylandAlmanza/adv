#ifndef PLAYGAME_H_
#define PLAYGAME_H_

#include "scene.h"

void adv_play_game_init(adv_display *display);
void adv_play_game_update(adv_display *display, int key);
void adv_play_game_uninit();

#endif
