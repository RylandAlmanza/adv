#ifndef LUAUTILS_H
#define LUAUTILS_H

#include <stdbool.h>
#include <lua.h>
#include <lauxlib.h>

void adv_lua_init_state(lua_State *L, char *filename);
int adv_lua_get_int_field(lua_State *L, char *key);
char *adv_lua_get_str_field(lua_State *L, char *key);
bool adv_lua_get_bool_field(lua_State *L, char *key);
void adv_lua_select(lua_State *L, char *key);
void adv_lua_uninit_state(lua_State *L);

#endif
