#ifndef MENU_H_
#define MENU_H_

#include "interface.h"

typedef struct adv_menu_option_struct adv_menu_option;

struct adv_menu_option_struct {
    char *label;
    void (*callback)(adv_display *display);
};

typedef struct adv_menu_struct adv_menu;

struct adv_menu_struct {
    int selection;
    int option_count;
    adv_menu_option *options;

    void (*add_option)(adv_menu *self,
                       char *label,
                       void (*callback)(adv_display *display));
};

adv_menu adv_menu_new();

#endif
