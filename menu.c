#include <stdlib.h>
#include "menu.h"

void adv_menu_add_option(adv_menu *self,
                         char *label,
                         void (*callback)(adv_display *display)) {
    adv_menu_option option = {
        .label = label,
        .callback = callback
    };

    self->option_count++;
    self->options = realloc(self->options,
                            sizeof(adv_menu_option) * self->option_count);
    self->options[self->option_count - 1] = option;
}

adv_menu adv_menu_new() {
    adv_menu menu;
    menu.selection = 0;
    menu.option_count = 0;
    menu.options = malloc(sizeof(adv_menu_option));

    menu.add_option = &adv_menu_add_option;

    return menu;
}
