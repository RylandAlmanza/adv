#include "startmenu.h"
#include "playgame.h"
#include "menu.h"
#include "actions.h"

adv_menu adv_start_menu;

void start_game(adv_display *display) {
    adv_set_scene(&adv_play_game_init,
                  &adv_play_game_update,
                  &adv_play_game_uninit);
    display->wipe();
    adv_init_scene(display);
}

void quit_game(adv_display *display) {
    display->draw('X', 7, 1, WHITE, BLACK);
}

void adv_start_menu_init(adv_display *display) {
    adv_start_menu = adv_menu_new();
    adv_start_menu.add_option(&adv_start_menu, "Start", &start_game);
    adv_start_menu.add_option(&adv_start_menu, "Quit", &quit_game);

    int i;
    for (i = 0; i < adv_start_menu.option_count; i++) {
        display->draw_string(adv_start_menu.options[i].label,
                             2,
                             i,
                             WHITE,
                             BLACK);
    }
    display->draw('>', 0, adv_start_menu.selection, WHITE, BLACK);
    display->flush();
}

void update_selection(adv_display *display, int delta) {
    display->draw(' ', 0, adv_start_menu.selection, WHITE, BLACK);
    adv_start_menu.selection += delta;
    if (adv_start_menu.selection < 0) {
        adv_start_menu.selection = adv_start_menu.option_count - 1;
    }
    if (adv_start_menu.selection >= adv_start_menu.option_count) {
        adv_start_menu.selection = 0;
    }
    display->draw('>', 0, adv_start_menu.selection, WHITE, BLACK);
    display->flush();
}

void adv_start_menu_update(adv_display *display, int key) {
    if (key == adv_action_move_down) {
        update_selection(display, 1);
    }
    if (key == adv_action_move_up) {
        update_selection(display, -1);
    }
    if (key == adv_action_select) {
        adv_start_menu.options[adv_start_menu.selection].callback(display);
    }
}

void adv_start_menu_uninit() {

}
