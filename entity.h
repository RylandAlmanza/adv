#ifndef ENTITY_H_
#define ENTITY_H_

#include <stdbool.h>
#include "sprites.h"

typedef struct adv_entity_struct adv_entity;

struct adv_entity_struct {
    adv_sprite sprite;
    int x;
    int y;
    int facing;
    bool is_solid;

    void (*move)(adv_entity *self, int direction);
    void (*move_back)(adv_entity *self);
    void (*update)(adv_entity *self);
};

void adv_entity_init();
adv_entity adv_entity_new(int x, int y, char *sprite);
adv_entity adv_entity_get(char *entity_name);
adv_entity adv_tile_get(char tile_symbol);
void adv_entity_uninit();

#endif
