local black = 0;
local red = 1;
local green = 2;
local yellow = 3;
local blue = 4;
local magenta = 5;
local cyan = 6;
local white = 7;

local entities = {
    player = {
        sprite = {
            character = '@',
            fg = white,
            bg = black
        },
        is_solid = true
    },
    tree = {
        sprite = {
            character = 'T',
            fg = green,
            bg = black
        },
        is_solid = true
    },
    grass = {
        sprite = {
            character = '.',
            fg = green,
            bg = black
        },
        is_solid = false
    },
    water = {
        sprite = {
            character = '~',
            fg = blue,
            bg = black
        },
        is_solid = false
    }
}

local tile_symbols = {
    T = 'tree',
    ['.'] = 'grass',
    ['~'] = 'water'
}

return {entities = entities, tile_symbols = tile_symbols}
