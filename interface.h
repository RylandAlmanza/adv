#ifndef INTERFACE_H_
#define INTERFACE_H_

#include <ncurses.h>

static const int BLACK = COLOR_BLACK;
static const int YELLOW = COLOR_YELLOW;
static const int GREEN = COLOR_GREEN;
static const int BLUE = COLOR_BLUE;
static const int WHITE = COLOR_WHITE;
static const int TRANSPARENT = -1;

typedef struct adv_display_struct adv_display;

struct adv_display_struct {
    void (*init)();
    void (*draw)(char sprite, int x, int y, int fg, int bg);
    void (*draw_string)(char *str, int x, int y, int fg, int bg);
    void (*flush)();
    void (*wipe)();
    void (*uninit)();
};

adv_display adv_display_new();

#endif
