#include <stdlib.h>
#include "luautils.h"

void adv_lua_init_state(lua_State *L, char *filename) {
    int status, result;

    luaL_openlibs(L);

    status = luaL_loadfile(L, filename);
    if (status) {
        fprintf(stderr, "Couldn't load file: %s\n", lua_tostring(L, -1));
        exit(1);
    }

    result = lua_pcall(L, 0, LUA_MULTRET, 0);
    if (result) {
        fprintf(stderr, "Failed to run script: %s\n", lua_tostring(L, -1));
        exit(1);
    }
}

int adv_lua_get_int_field(lua_State *L, char *key) {
    int result;
    lua_pushstring(L, key);
    lua_gettable(L, -2);
    if (lua_isnil(L, -1)) {
        result = 0;
    } else {
        result = (int)lua_tonumber(L, -1);
    }
    lua_pop(L, 1);
    return result;
}

char *adv_lua_get_str_field(lua_State *L, char *key) {
    char *result;
    lua_pushstring(L, key);
    lua_gettable(L, -2);
    result = (char *)lua_tostring(L, -1);
    lua_pop(L, 1);
    return result;
}

bool adv_lua_get_bool_field(lua_State *L, char *key) {
    bool result;
    lua_pushstring(L, key);
    lua_gettable(L, -2);
    result = lua_toboolean(L, -1);
    lua_pop(L, 1);
    return result;
}

void adv_lua_select(lua_State *L, char *key) {
    lua_pushstring(L, key);
    lua_gettable(L, -2);
}

void adv_lua_uninit_state(lua_State *L) {
    lua_pop(L, 1);
    lua_close(L);
}
