CC=gcc
LIBS=-lncurses -llua
DEPS=*.h

main: $(DEPS) *c
	$(CC) -g *.c -o main $(LIBS)
