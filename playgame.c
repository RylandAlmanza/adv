#include <stdbool.h>
#include <stdlib.h>
#include "playgame.h"
#include "entity.h"
#include "directions.h"
#include "luautils.h"

#undef move no_ncurses_I_dont_fucking_mean_wmove_god_fucking_dammit

adv_entity *entities;
int entity_count = 0;
adv_entity *player;
int player_id = 0;
int map_width, map_height;
adv_entity **map;

void load_map() {
    int status, result, x, y;
    bool is_solid;
    char *sprite;
    char *data;
    lua_State *L;
    L = luaL_newstate();

    adv_lua_init_state(L, "map.lua");
    map_width = adv_lua_get_int_field(L, "width");
    map_height = adv_lua_get_int_field(L, "height");
    data = adv_lua_get_str_field(L, "data");

    map = malloc(sizeof(adv_entity) * (map_width * map_height));
    for (y = 0; y < map_height; y++) {
        map[y] = malloc(sizeof(adv_entity) * map_width);
        for (x = 0; x < map_width; x++) {
            char tile[2];
            //tile[0] = data[(y * map_width) + x];
            //tile[1] = 0;
            map[y][x] = adv_tile_get(data[(y * map_width) + x]);
        }
    }

    adv_lua_uninit_state(L);
}

int add_entity(adv_entity entity) {
    entity_count++;
    entities = realloc(entities, sizeof(adv_entity) * (entity_count + 1));
    entities[entity_count - 1] = entity;
    return entity_count - 1;
}

void draw_entity(adv_display *display, int id) {
    display->draw(entities[id].sprite.character,
                  entities[id].x,
                  entities[id].y,
                  entities[id].sprite.fg,
                  entities[id].sprite.bg);
}

void draw_tile(adv_display *display, int x, int y) {
    display->draw(map[y][x].sprite.character,
                  x,
                  y,
                  map[y][x].sprite.fg,
                  map[y][x].sprite.bg);
}

void adv_play_game_init(adv_display *display) {
    adv_entity_init();
    load_map();
    int x, y;
    for (y = 0; y < map_height; y++) {
        for (x = 0; x < map_width; x++) {
            draw_tile(display, x, y);
        }
    }
    add_entity(adv_entity_get("player"));
    player = &entities[player_id];
    player->x = 1;
    player->y = 1;
    draw_entity(display, player_id);
    display->flush();
}

void adv_play_game_update(adv_display *display, int key) {
    int direction = adv_key_to_direction(key);
    draw_tile(display, player->x, player->y);
    if (direction != adv_invalid_direction) {
        player->move(player, direction);
        if (map[player->y][player->x].is_solid) {
            player->move_back(player);
        }
    }
    draw_entity(display, player_id);
    display->flush();
}

void adv_play_game_uninit() {
    int y;
    for (y = 0; y < map_height; y++) {
        free(map[y]);
    }
    free(map);
    adv_entity_uninit();
}
