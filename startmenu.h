#ifndef STARTMENU_H_
#define STARTMENU_H_

#include "interface.h"

void adv_start_menu_init(adv_display *display);
void adv_start_menu_update(adv_display *display, int key);
void adv_start_menu_uninit();

#endif
