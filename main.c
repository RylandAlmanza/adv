#include <stdbool.h>
#include "interface.h"
#include "scene.h"
#include "startmenu.h"
#include "luautils.h"

adv_display display;

int main() {
    int game_over = false;
    display = adv_display_new();
    display.init();

    lua_State *adv_keys_L;
    adv_keys_L = luaL_newstate();
    adv_lua_init_state(adv_keys_L, "keys.lua");

    adv_set_scene(&adv_start_menu_init,
                  &adv_start_menu_update,
                  &adv_start_menu_uninit);
    adv_init_scene(&display);

    int ch;
    int key;
    while (ch != 'q' && game_over == false) {
        ch = getch();
        if (ch == ERR) {
            ch = 0;
        }
        char *key_name;
        if (ch == 0403) {
            key_name = "up";
        } else if (ch == 0405) {
            key_name = "right";
        } else if (ch == 0402) {
            key_name = "down";
        } else if (ch == 0404) {
            key_name = "left";
        } else {
            sprintf(key_name, "%c", ch);
        }
        key = adv_lua_get_int_field(adv_keys_L, key_name);

        adv_update_scene(&display, key);
    }

    adv_lua_uninit_state(adv_keys_L);

    adv_uninit_scene();

    display.uninit();
};
