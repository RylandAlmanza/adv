MOVE_UP = 1
MOVE_RIGHT = 2
MOVE_DOWN = 3
MOVE_LEFT = 4
SELECT = 5

key_bindings = {
    up = MOVE_UP,
    right = MOVE_RIGHT,
    down = MOVE_DOWN,
    left = MOVE_LEFT,
    k = MOVE_UP,
    l = MOVE_RIGHT,
    j = MOVE_DOWN,
    h = MOVE_LEFT,
    x = SELECT
}

return key_bindings
