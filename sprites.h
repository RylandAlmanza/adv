#ifndef SPRITE_H_
#define SPRITE_H_

typedef struct adv_sprite_struct adv_sprite;

struct adv_sprite_struct {
    char character;
    int fg;
    int bg;
};

void adv_sprites_init();
adv_sprite adv_sprites_get(char *sprite_name);
void adv_sprites_uninit();

#endif
